//
//  AppDelegate.m
//  MotionDetection
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "AppDelegate.h"
#import "TKLocationManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[TKLocationManager sharedInstance] startSignificant];
    
    return YES;
}
							
@end
