//
//   TKWeakTimerTarget.h
//  NSTimerTest
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GCDTimer

-(void)timerFired:(id)sender;

@end

@interface  TKWeakTimerTarget : NSObject

@property (nonatomic, weak) id<GCDTimer>delegate;

-(id)initWithInterval:(NSTimeInterval)interval;

- (void)startTimer;

- (void)cancelTimer;

- (NSInteger)getInstances;

@end
