//
//  ViewController.m
//  MotionDetection
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "ViewController.h"
#import "TKMotionDetector.h"
#import "TKStepDetector.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *vAccuracyLabel;
@property (weak, nonatomic) IBOutlet UILabel *hAccuracyLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *motionLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *motionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *isShakingLabel;
@property NSInteger stepCount;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __weak ViewController *weakSelf = self;
    
    [TKMotionDetector sharedInstance].motionTypeChangedBlock = ^(TKMotionType motionType) {
        NSString *type = @"";
        switch (motionType) {
            case MotionTypeNotMoving:
                type = @"Not moving";
                break;
            case MotionTypeWalking:
                type = @"Walking";
                break;
            case MotionTypeRunning:
                type = @"Running";
                break;
            case MotionTypeAutomotive:
                type = @"Automotive";
                break;
        }
        
        [self changMotionLabelValue:type];
    };
    
    [TKMotionDetector sharedInstance].momentSpeedChangedBlock = ^(float speed){
        [self changeMotionSpeedValue:speed];
    };
    
    [TKMotionDetector sharedInstance].locationChangedBlock = ^(CLLocation *location) {
        
        [self changeSpeedLabelValue:location.speed * 3.6f];
        [self changeHorizontalAccuracyLabelValue:location.horizontalAccuracy];
        [self changeVerticalAccuraryLabelValue:location.verticalAccuracy];
    };
    
    [TKMotionDetector sharedInstance].accelerationChangedBlock = ^(CMAcceleration acceleration) {
        BOOL isShaking = [TKMotionDetector sharedInstance].isShaking;
        
        if (isShaking && [weakSelf.isShakingLabel.text isEqualToString:@"not shaking"])
        {
            [self changeShakingLabel:isShaking];
        }
    };
    
    [TKMotionDetector sharedInstance].useM7IfAvailable = YES;
    
    [TKLocationManager sharedInstance].allowsBackgroundLocationUpdates = YES;
    
    //Starting motion detector
    [[TKMotionDetector sharedInstance] startDetection];
    
    //Starting podometer
    [[TKStepDetector sharedInstance] startDetectionWithUpdateBlock:^(NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
            return;
        }
        
        weakSelf.stepCount++;
        
        [self changeStepCounterLabelValue:weakSelf.stepCount];
    }];
}


#pragma mark TWEAK UI

- (void) changeSpeedLabelValue:(float)value
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.speedLabel.text = [NSString stringWithFormat:@"%.2f km/h",value];
    });
}

- (void) changeHorizontalAccuracyLabelValue:(float)value
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.hAccuracyLabel.text = [NSString stringWithFormat:@"H %.2f mts",value];
    });
}

- (void) changeVerticalAccuraryLabelValue:(float)value
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.vAccuracyLabel.text = [NSString stringWithFormat:@"V %.2f mts",value];
    });
}

- (void) changeMotionSpeedValue:(float)value
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.motionLabel.text = [NSString stringWithFormat:@"%.3f m/s",value];
    });
}

- (void) changeStepCounterLabelValue:(NSInteger)value
{
    self.stepCountLabel.text = [NSString stringWithFormat:@"Step count: %li", (long)value];
}

- (void) changMotionLabelValue:(NSString *)text
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.motionTypeLabel.text = text;
    });
}

- (void) changeShakingLabel:(BOOL)isShaking
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.isShakingLabel.text = isShaking ? @"shaking":@"not shaking";
    });
}



@end
