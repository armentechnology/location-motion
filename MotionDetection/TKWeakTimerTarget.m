//
//   TKWeakTimerTarget.m
//  NSTimerTest
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "TKWeakTimerTarget.h"

dispatch_source_t CreateDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block)
{
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer)
    {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

@interface  TKWeakTimerTarget()

@property NSTimeInterval fireInterval;

@property NSInteger counter;

@end

@implementation  TKWeakTimerTarget
{
    dispatch_source_t _timer;
}


- (id)initWithInterval:(NSTimeInterval)interval
{
    self = [super init];
    
    if (self)
    {
        if (!interval)
        {
            interval = 1.0f;
        }
        self.fireInterval = interval;
        self.counter = 1;
    }
    
    return self;
}

- (void)startTimer
{    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    __weak __typeof(self) weakSelf = self;
    
    _timer = CreateDispatchTimer(_fireInterval, queue, ^{
        [weakSelf.delegate timerFired:weakSelf];
        weakSelf.counter++;
    });
}

- (void)cancelTimer
{
    if (_timer)
    {
        dispatch_source_cancel(_timer);
        _timer = nil;
    }
}

- (NSInteger) getInstances
{
    return _counter;
}

@end
