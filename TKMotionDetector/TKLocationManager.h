//
//  TKMotionDetecter.h
//  MotionDetection
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

typedef enum
{
    LocationManagerTypeNone = 0x00,
    LocationManagerTypeStandart = 0x10,
    LocationManagerTypeSignificant = 0x01,
    LocationManagetTypeStandartAndSignificant = 0x11
} TKLocationManagerType;

@protocol TKLocationManagerDelegate <NSObject>

- (void) locationDidChange:(CLLocation *)location;
- (void) locationDidFail:(NSError *)error;
- (void) locationDidPause;
- (void) locationDidChangeAuthorization:(CLAuthorizationStatus)status;

@end

@interface TKLocationManager : NSObject <CLLocationManagerDelegate>

@property (weak, nonatomic) id<TKLocationManagerDelegate>delegate;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocationManager *significantLocationManager;

@property (strong, nonatomic) CLLocation* lastLocation;
@property (nonatomic) CLLocationCoordinate2D lastCoordinate;

@property (nonatomic) BOOL allowsBackgroundLocationUpdates;

+ (TKLocationManager *)sharedInstance;

/**
 *  Indicates in whether of LocationManagetType state is now the location manager's shared instance.
 */
@property (nonatomic) TKLocationManagerType locationType;

/**
 * Start Location Update
 */
- (void)start;

/**
 * Start Significant Location Update
 */
- (void)startSignificant;

/**
 * Stop Standard Location Update only (this means, if significant update is also started, then will be stopped standard location updates only).
 */
- (void)stop;

/**
 * Stop Significant Location Update (only)
 */
- (void)stopSignificant;

@end
