//
//  TKMotionDetecter.h
//  MotionDetection
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKStepDetector : NSObject

+ (instancetype)sharedInstance;

/**
 * Start accelerometer updates.
 * @param callback Will be called every time when new step is detected
 */
- (void)startDetectionWithUpdateBlock:(void(^)(NSError *error))callback;

/**
 * Stop motion manager accelerometer updates
 */
- (void)stopDetection;

@end
