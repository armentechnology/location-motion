//
//  TKLocationManager.m
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "TKLocationManager.h"

@implementation TKLocationManager

- (id)init
{
    self = [super init];
    if (self)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        self.locationManager.delegate = self;
        
        self.significantLocationManager = [[CLLocationManager alloc] init];
        self.significantLocationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.significantLocationManager.distanceFilter = kCLDistanceFilterNone;
        self.significantLocationManager.delegate = self;
        
        self.locationType = LocationManagerTypeNone;
    }
    
    return self;
}

+ (TKLocationManager *)sharedInstance
{
    __strong static TKLocationManager* instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (void)start
{
    if (self.allowsBackgroundLocationUpdates) {
        if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
        }
    }
    [self.locationManager startUpdatingLocation];
    
    if (self.locationType == LocationManagerTypeNone) {
        self.locationType = LocationManagerTypeStandart;
    } else if (self.locationType == LocationManagerTypeSignificant) {
        self.locationType = LocationManagerTypeSignificant | LocationManagerTypeStandart;
    }
}

- (void)startSignificant
{
    if (self.allowsBackgroundLocationUpdates) {
        if ([self.significantLocationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
            [self.significantLocationManager setAllowsBackgroundLocationUpdates:YES];
        }
    }
    
    [self.significantLocationManager startMonitoringSignificantLocationChanges];
    
    if (self.locationType == LocationManagerTypeNone) {
        self.locationType = LocationManagerTypeSignificant;
    } else if (self.locationType == LocationManagerTypeStandart) {
        self.locationType = LocationManagerTypeStandart | LocationManagerTypeSignificant;
    }
}

- (void)stop
{
    [self.locationManager stopUpdatingLocation];
    
    if (self.locationType & LocationManagerTypeSignificant) {
        //leave only significant
        self.locationType = LocationManagerTypeSignificant;
    } else {
        self.locationType = LocationManagerTypeNone;
    }
}

- (void)stopSignificant
{
    [self.locationManager stopMonitoringSignificantLocationChanges];

    if (self.locationType & LocationManagerTypeStandart) {
        //leave only significant
        self.locationType = LocationManagerTypeStandart;
    } else {
        self.locationType = LocationManagerTypeNone;
    }
}

- (void)setAllowsBackgroundLocationUpdates:(BOOL)allowsBackgroundLocationUpdates
{
    _allowsBackgroundLocationUpdates = allowsBackgroundLocationUpdates;
    if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
        [self.locationManager setAllowsBackgroundLocationUpdates:allowsBackgroundLocationUpdates];
        [self.significantLocationManager setAllowsBackgroundLocationUpdates:allowsBackgroundLocationUpdates];
    }
}

#pragma mark - CLLocatiomManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"Update location");
    
    if (manager == self.locationManager) {
        CLLocation *location = [locations lastObject];
        self.lastLocation = location;
        self.lastCoordinate = location.coordinate;
        [self.delegate locationDidChange:location];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self.delegate locationDidFail:error];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined && [self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.delegate locationDidChangeAuthorization:status];
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    [self.delegate locationDidPause];
}

@end
