//
//  MotionDetecter.m
//  MotionDetection
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "TKMotionDetector.h"
#import "TKWeakTimerTarget.h"

#define kAccelerometerNoiseAttenuation          3.0
#define kAccelerometerMinStep                   0.02
#define kMinToTriggerAccelSpeed                 0.2

#define rate 5.0f
#define freq 60.0f;

double Norm(double x, double y, double z)
{
    return sqrt(x * x + y * y + z * z);
}

double Clamp(double v, double min, double max)
{
    if(v > max)
        return max;
    else if(v < min)
        return min;
    else
        return v;
}

double filterConstant;

CGFloat kMinimumSpeed        = 0.3f;
CGFloat kMaximumWalkingSpeed = 1.9f;
CGFloat kMaximumRunningSpeed = 7.5f;
CGFloat kMinimumRunningAcceleration = 3.5f;

@interface TKMotionDetector() <GCDTimer, TKLocationManagerDelegate>

@property (strong, nonatomic) NSTimer *shakeDetectingTimer;

@property (nonatomic) TKMotionType previousMotionType;
@property  TKWeakTimerTarget *timer;

#pragma mark - Accelerometer manager
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong, nonatomic) CMMotionActivityManager *motionActivityManager;


@end

@implementation TKMotionDetector

+ (TKMotionDetector *)sharedInstance
{
    static TKMotionDetector *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init
{
    self = [super init];
    if (self) {

        self.motionManager = [[CMMotionManager alloc] init];
        
        double dt = 1.0 / rate;
        double RC = 1.0 / freq;
        filterConstant = dt / (dt + RC);
    }
    
    return self;
}

+ (BOOL)motionHardwareAvailable
{
    static BOOL isAvailable = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isAvailable = [CMMotionActivityManager isActivityAvailable];
    });
    
    return isAvailable;
}

#pragma mark - Public Methods
- (void)startDetection
{
    __weak __typeof(self) weakSelf = self;
    
    [[TKLocationManager sharedInstance] start];
    [[TKLocationManager sharedInstance] setDelegate:self];
    
    _timer = [[ TKWeakTimerTarget alloc]initWithInterval:0.2f];
    _timer.delegate = self;
    [_timer startTimer];

    
    [self.motionManager startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
                                             withHandler:^(CMAccelerometerData *accelerometerData, NSError *error)
    {
         weakSelf.acceleration = accelerometerData.acceleration;

        dispatch_async(dispatch_get_main_queue(), ^{
             if (self.accelerationChangedBlock) {
                 self.accelerationChangedBlock (self.acceleration);
             }
         });
     }];
    
    if (self.useM7IfAvailable && [TKMotionDetector motionHardwareAvailable]) {
        if (!self.motionActivityManager) {
            self.motionActivityManager = [[CMMotionActivityManager alloc] init];
        }
        
        [self.motionActivityManager startActivityUpdatesToQueue:[[NSOperationQueue alloc] init] withHandler:^(CMMotionActivity *activity) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (activity.walking) {
                    weakSelf.motionType = MotionTypeWalking;
                } else if (activity.running) {
                    weakSelf.motionType = MotionTypeRunning;
                } else if (activity.automotive) {
                    weakSelf.motionType = MotionTypeAutomotive;
                } else if (activity.stationary || activity.unknown) {
                    weakSelf.motionType = MotionTypeNotMoving;
                }
                
                // If type was changed, then call delegate method
                if (self.motionType != self.previousMotionType) {
                    self.previousMotionType = self.motionType;
                    if (self.motionTypeChangedBlock) {
                        self.motionTypeChangedBlock (self.motionType);
                    }
                }
            });
        }];
    }
}

- (void)stopDetection
{
    [self.shakeDetectingTimer invalidate];
    self.shakeDetectingTimer = nil;
    
    [[TKLocationManager sharedInstance] stop];
    [self.motionManager stopAccelerometerUpdates];
    [self.motionActivityManager stopActivityUpdates];
}

#pragma mark - Customization Methods
- (void)setMinimumSpeed:(CGFloat)speed
{
    kMinimumSpeed = speed;
}

- (void)setMaximumWalkingSpeed:(CGFloat)speed
{
    kMaximumWalkingSpeed = speed;
}

- (void)setMaximumRunningSpeed:(CGFloat)speed
{
    kMaximumRunningSpeed = speed;
}

- (void)setMinimumRunningAcceleration:(CGFloat)acceleration
{
    kMinimumRunningAcceleration = acceleration;
}

- (void)detectShaking
{
    //Array for collecting acceleration for last one seconds period.
    static NSMutableArray *shakeDataForOneSec = nil;
    //Counter for calculating completion of one second interval
    static float currentFiringTimeInterval = 0.0f;
    
    currentFiringTimeInterval += 0.01f;
    if (currentFiringTimeInterval < 1.0f) {// if one second time intervall not completed yet
        if (!shakeDataForOneSec)
            shakeDataForOneSec = [NSMutableArray array];
        
        // Add current acceleration to array
        NSValue *boxedAcceleration = [NSValue value:&_acceleration withObjCType:@encode(CMAcceleration)];
        [shakeDataForOneSec addObject:boxedAcceleration];
    } else {
        // Now, when one second was elapsed, calculate shake count in this interval. If there will be at least one shake then
        // we'll determine it as shaked in all this one second interval.
        
        int shakeCount = 0;
        for (NSValue *boxedAcceleration in shakeDataForOneSec) {
            CMAcceleration acceleration;
            [boxedAcceleration getValue:&acceleration];
         
            /*********************************
             *       Detecting shaking
             *********************************/
            double accX_2 = powf(acceleration.x,2);
            double accY_2 = powf(acceleration.y,2);
            double accZ_2 = powf(acceleration.z,2);
            
            double vectorSum = sqrt(accX_2 + accY_2 + accZ_2);
            
            if (vectorSum >= kMinimumRunningAcceleration) {
                shakeCount++;
            }
            /*********************************/
        }
        _isShaking = shakeCount > 0;
        
        shakeDataForOneSec = nil;
        currentFiringTimeInterval = 0.0f;
    }
}

- (void) detectInstantSpeed
{
//    double alpha = filterConstant;
    
    UIAccelerationValue x,y,z;
    
    x = powf(_acceleration.x,2);
    y = powf(_acceleration.y,2);
    z = powf(_acceleration.z,2);
    
//    double d = Clamp(fabs(Norm(x, y, z) - Norm(_acceleration.x, _acceleration.y, _acceleration.z)) / kAccelerometerMinStep - 1.0, 0.0, 1.0);
//    alpha = (1.0 - d) * filterConstant / kAccelerometerNoiseAttenuation + d * filterConstant;
//
//    x = _acceleration.x * alpha + x * (1.0 - alpha);
//    y = _acceleration.y * alpha + y * (1.0 - alpha);
//    z = _acceleration.z * alpha + z * (1.0 - alpha);
    
    double vectorSum = sqrt(x + y + z);
    
    float speed = vectorSum * 9.81 - 9.81;
    
    speed = sqrt(speed * speed);
    
    if (self.momentSpeedChangedBlock)
    {
        self.momentSpeedChangedBlock(speed > kMinToTriggerAccelSpeed ? speed : 0);
    }
}

#pragma mark LOCATION DELEGATE

- (void) locationDidChange:(CLLocation *)location
{
    _currentSpeed = location.speed;
    if (_currentSpeed < 0) {
        _currentSpeed = 0;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.locationChangedBlock) {
            self.locationChangedBlock (location);
        }
    });
}

- (void) locationDidPause
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.locationWasPausedBlock) {
            self.locationWasPausedBlock (TRUE);
        }
    });
}

- (void) locationDidFail:(NSError *)error
{
    
}

- (void) locationDidChangeAuthorization:(CLAuthorizationStatus)status
{
    
}


#pragma mark  TKTIMER DELEGATE

- (void)timerFired:(id)sender
{
    [self detectInstantSpeed];
    
    [self detectShaking];
}

@end
