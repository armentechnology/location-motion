//
//  TKMotionDetecter.h
//  MotionDetection
//
//  Created by Jose Catala on 11/09/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TKLocationManager.h"
#import <CoreMotion/CoreMotion.h>

@class TKMotionDetector;
typedef enum
{
  MotionTypeNotMoving = 1,
  MotionTypeWalking,
  MotionTypeRunning,
  MotionTypeAutomotive
} TKMotionType;



@interface TKMotionDetector : NSObject

#pragma mark - Singleton
+ (TKMotionDetector *)sharedInstance;

#pragma mark - Properties
@property (copy) void (^motionTypeChangedBlock) (TKMotionType motionType);
@property (copy) void (^locationChangedBlock) (CLLocation *location);
@property (copy) void (^accelerationChangedBlock) (CMAcceleration acceleration);
@property (copy) void (^momentSpeedChangedBlock) (float speed);
@property (copy) void (^locationWasPausedBlock) (BOOL changed);

@property TKMotionType motionType;
@property  double currentSpeed;
@property  CMAcceleration acceleration;
@property  BOOL isShaking;


#pragma mark - Methods
- (void)startDetection;
- (void)stopDetection;

#pragma mark - Customization Methods

/**
 * Set this parameter to YES if you want to use M7 chip to detect more exact motion type. By default is No.
 * Set this parameter before calling startDetection method.
 * Available only on devices that have M7 chip. At this time only the iPhone 5S, iPhone6/6plus, the iPad Air and iPad mini with retina display have the M7 coprocessor.
 */
@property (nonatomic) BOOL useM7IfAvailable NS_AVAILABLE_IOS(7_0);


/**
 *@param speed  The minimum speed value less than which will be considered as not moving state
 */
- (void)setMinimumSpeed:(CGFloat)speed;

/**
 *@param speed  The maximum speed value more than which will be considered as running state
 */
- (void)setMaximumWalkingSpeed:(CGFloat)speed;

/**
 *@param speed  The maximum speed value more than which will be considered as automotive state
 */
- (void)setMaximumRunningSpeed:(CGFloat)speed;

/**
 *@param acceleration  The minimum acceleration value less than which will be considered as non shaking state
 */
- (void)setMinimumRunningAcceleration:(CGFloat)acceleration;


@end
